﻿/*
 * Server Calculator
 * 
 * Bayu Prajanata (4210171014)
 */

using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets; //for opening sockets
using System.Text;
using System.Threading; //for handling multithreading

namespace server{
	//base class for processing client request
	class BaseProcess {
		public static void Main(String[] argument){
			TcpListener Server = null;
			bool WaitingClient = true;
			//count how many client have enter
			int ClientCounter = 0 ;
			try{
				//setting port
				Int32 Port = 8080;
				//create a way for clients to enter
				Server = new TcpListener(IPAddress.Any, Port);
				//open the way for clients to enter
				Server.Start();
				
				while(WaitingClient){
					Console.WriteLine("Waiting for the client(s)...");
					//perform a blocking call to accept clients requests
					TcpClient Client = Server.AcceptTcpClient();
					ClientCounter++;
					Console.WriteLine("Client No. {0} has connected.", ClientCounter);
					
					//beginning to enter the process
					ThreadHandler ClientThread = new ThreadHandler();
					ClientThread.StartClient(Client, Convert.ToString(ClientCounter));
				}
			}
			catch (Exception Etc){
				Console.WriteLine(Etc.ToString());
			}
		}
	}
	
	//secondary class for handling multithreading
	class ThreadHandler{
		TcpClient ClientSocket;
		string ClientOrder;
		
		public void StartClient(TcpClient ThisSocket, string ThisOrder){
			Thread.Sleep(7);
			this.ClientSocket = ThisSocket;
			this.ClientOrder = ThisOrder;
			Thread ClientThread = new Thread(Parameter);
			ClientThread.Start();
		}
		
		private void Parameter(){
			byte[] BytesRecieve = new byte[100000025];
			string ClientData = null;
			byte[] BytesSend = null;
			string Messege = null;
			float FinalSum;
			NetworkStream StreamData = null;
			bool WaitingStream = true;
			
			while(WaitingStream){
				try{
					//get a stream object for reading and waiting
					StreamData = ClientSocket.GetStream();
					StreamData.Read(BytesRecieve, 0, (int)ClientSocket.ReceiveBufferSize);
					ClientData = System.Text.Encoding.ASCII.GetString(BytesRecieve);
					FinalSum = Calculator(ClientData);
					Messege = "The summary is " + Convert.ToString(FinalSum);
					Console.WriteLine(Messege);
					BytesSend = System.Text.Encoding.ASCII.GetBytes(Messege);
					StreamData.Write(BytesSend, 0, BytesSend.Length);
				}
				
				catch(Exception Etc){
					StreamData.Close();
					ClientSocket.Close();
					WaitingStream = false;
					Console.WriteLine(Etc.ToString());
				}
			}
		}
		
		//calculation function
		private float Calculator(string CalculationArgument){
			string Argument = CalculationArgument;
			float ConstOne;
			float ConstTwo;
			char Operand;
			float Sum = 0;
			
			//split argument with a space
			string[] SplitArgument = Argument.Split(' ');
			ConstOne = Convert.ToSingle(SplitArgument[0]);
			Operand = char.Parse(SplitArgument[1]);
			ConstTwo = Convert.ToSingle(SplitArgument[2]);
			
			switch(Operand){
				case '+':
					Sum = ConstOne + ConstTwo;
					break;
				case '-':
					Sum = ConstOne - ConstTwo;
					break;
				case '*':
					Sum = ConstOne * ConstTwo;
					break;
				case '/':
					Sum = ConstOne / ConstTwo;
					break;
				case '^':
					Sum = Convert.ToSingle(Math.Pow(ConstOne, ConstTwo));
					break;
				case '%':
					Sum = ConstOne % ConstTwo;
					break;
				
			}
			return Sum;
		}
	}
}