﻿/*
 * Client Calculator
 * 
 * Bayu Prajanata (4210171014)
 * 
 */
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

class client {
	public static void Main(String[] argument)
	{
		string Messege;
		byte[] Data;
		//create TcpClient
		Int32 Port = 8080;
		TcpClient Client = new TcpClient("127.0.0.1", Port);
		
		try{
			//get a client stream for reading and writing
			NetworkStream StreamData = Client.GetStream();
			Console.WriteLine("Connected to Server");
			
			while(true){
				Console.WriteLine("Input equation: ");
				Messege = Console.ReadLine();
				//change the messege from strings into bytes
				Data = System.Text.Encoding.ASCII.GetBytes(Messege);
				//sending messege to TcpListener
				StreamData.Write(Data, 0, Data.Length);
				Console.WriteLine("Sent: {0}", Messege);
				//recieving messege from server
				Data = new Byte[256];
				String ServerData = String.Empty;
				Int32 BytesRecieve = StreamData.Read(Data, 0, Data.Length);
				ServerData = System.Text.Encoding.ASCII.GetString(Data, 0, BytesRecieve);
				Console.WriteLine(ServerData);
			}
		}
		
		catch (ArgumentNullException Etc){
			Console.WriteLine("ArgumentNullException: {0}", Etc);
		}
		
		Console.WriteLine("\n Press 'Enter' to continue...");
		Console.Read();
	}
}